<?php

declare(strict_types=1);

namespace Dexodus\EntityStatisticsBundle\DependencyInjection;

use Dexodus\EntityStatisticsBundle\Service\EntityStatisticsLoader;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('entity_statistics');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('loader')
                    ->defaultValue(EntityStatisticsLoader::class)
                ->end()
                ->arrayNode('mapping')->arrayPrototype()->children()
                    ->scalarNode('dir')
                        ->cannotBeEmpty()
                    ->end()
                    ->scalarNode('prefix')
                        ->cannotBeEmpty()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
