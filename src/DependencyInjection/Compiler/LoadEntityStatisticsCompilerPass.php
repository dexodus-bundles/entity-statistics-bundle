<?php

declare(strict_types=1);

namespace Dexodus\EntityStatisticsBundle\DependencyInjection\Compiler;

use Dexodus\EntityStatisticsBundle\Attribute\EntityStatistics as EntityStatisticsAttribute;
use Dexodus\EntityStatisticsBundle\Service\EntityStatisticsLoaderInterface;
use Exception;
use ReflectionAttribute;
use ReflectionClass;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class LoadEntityStatisticsCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $loaderClass = $container->getParameter('entity-statistics.loader_class');

        if (!$container->hasDefinition($loaderClass)) {
            $container->setDefinition($loaderClass, new Definition($loaderClass));
        }

        $container->setAlias(EntityStatisticsLoaderInterface::class, $loaderClass);
        $loader = $container->findDefinition(EntityStatisticsLoaderInterface::class);

        foreach ($container->getParameter('entity-statistics.mapping') as $mapping) {
            $potentialEntityStatisticsPaths = scandir($mapping['dir']);

            if ($potentialEntityStatisticsPaths === false) {
                throw new Exception("Directory '{$mapping['dir']}' not found for load entity statistics");
            }

            $this->scanDirectoryAndAddEntityStatisticsToLoader($mapping['dir'], $mapping['prefix'], $loader);
        }
    }

    private function scanDirectoryAndAddEntityStatisticsToLoader(string $directory, string $prefix, Definition $loader): void
    {
        $entityStatistics = [];

        foreach (scandir($directory) as $path) {
            if ($path === '.' || $path === '..') {
                continue;
            }

            $filepath = $directory . '/' . $path;
            $class = $prefix . '\\' . str_replace('.php', '', $path);

            if (!class_exists($class)) {
                if (is_dir($filepath)) {
                    $this->scanDirectoryAndAddEntityStatisticsToLoader($filepath, $prefix . '\\' . $path, $loader);
                }

                continue;
            }

            $entityStatisticsAttributes = $this->getEntityStatisticsAttributes($class);

            if (count($entityStatisticsAttributes) === 0) {
                continue;
            }

            $entityStatistics[] = $class;
        }

        $loader->addMethodCall('setEntityStatistics', [array_unique($entityStatistics)]);
    }

    /** @return ReflectionAttribute[] */
    private function getEntityStatisticsAttributes(string $class): array
    {
        $reflectionClass = new ReflectionClass($class);

        return $reflectionClass->getAttributes(EntityStatisticsAttribute::class);
    }
}
