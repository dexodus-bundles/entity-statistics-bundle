<?php

declare(strict_types=1);

namespace Dexodus\EntityStatisticsBundle\Service;

interface EntityStatisticsLoaderInterface
{
    public function setEntityStatistics(array $entityStatistics): void;
}
