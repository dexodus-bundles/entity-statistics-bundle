<?php

declare(strict_types=1);

namespace Dexodus\EntityStatisticsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class EntityStatisticsBundle extends Bundle
{
}
