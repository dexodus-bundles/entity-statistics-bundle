<?php

declare(strict_types=1);

namespace Dexodus\EntityStatisticsBundle\Controller;

use Dexodus\EntityStatisticsBundle\Service\EntityStatisticsLoaderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StatisticsController
{
    public function __construct(
        private EntityStatisticsLoaderInterface $entityStatisticsLoader,
    ) {
    }

    #[Route('/entity-statistics/{statisticsName}', methods: ['GET'])]
    public function view(string $statisticsName): Response
    {
        return new Response('ok');
    }
}
