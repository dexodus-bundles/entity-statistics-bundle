<?php

declare(strict_types=1);

namespace Dexodus\EntityStatisticsBundle\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class EntityStatistics
{
}
